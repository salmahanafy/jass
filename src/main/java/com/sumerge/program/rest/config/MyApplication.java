package com.sumerge.program.rest.config;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author Ahmed Anwar
 */
@ApplicationPath("Users")
public class MyApplication extends Application {
}
